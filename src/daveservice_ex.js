//
//app initilization
//this has to be in global level
SIGNUP_KEY= "ZGFHZSAleHBvmtU5NgEyNzc0NyA1Ng__"
ENTERPRISE_ID = "heidi_fashion_fit"
USER_ID= "ananth+fashion@i2ce.in"
HOST_URL= "https://dashboard.iamdave.ai"

var DS = new DaveService(HOST_URL, ENTERPRISE_ID, {"signup_apikey": SIGNUP_KEY});

DS.list()
var measurement_types;

function get_list_of_mesurements(user_id){
    
    DS.list("measurement_type", {"measurement_for": "<male or female>"}, function(objects){
        measurement_types = objects["data"]
        //show screen for individual measurement input screen
        // ob["measurement_ref"] image refrence for how to measure
        // ob["measurement_desc"] description for how to measure
        // ob["measurement_name"] title of measurement
    })
}



//1. post signup
//2. upload person image
//3. Update person with new image
//4. ds.list("measurement_type", {"measurement_for": "male", "_sort_by": "priority"}) // {"is_first": true, "data": [{..Neck}, {...}]}
//5. After noting down the value post to measurement {"person_id": ds.user_id, "measurement_type_id": ob["measurement_type_id"], "noted_value": 35}
//6. After all measurements are posted Show thank you screen

//Calculate BMI = Weight/((Height * 0.01)*(Height * 0.01)

function bmi_level(bmi){
    if(bmi < 18.5)
        return "Under Weight"
    else if (bmi < 25.0)
        return "Ideal"
    else if(bmi < 25.0)
        return "Over Weight"
    return "Fat"
}
//Find obesity_level = bmi_level(BMI)



function signup(data){
    DS.signup({
        "name": "",
        "phone_number": "",
        "email_id": "",
        "gender": "",
        "image": data["full_path"]
    },function(data){
        //show the name etc from data
        //....
        get_list_of_mesurements(DS.user_id);
    }, function(e){
        alert("Auth error")
    });
}


//Check already logged in? if not do signup
if(DS.sign_required){
    //collect the details from the first page and call signup
    
}else{
    get_list_of_mesurements(DS.user_id);
    //Can get user details add them to home page Like- name, etc...
    DS.get("person", DS.user_id, function(data){
        //show the name etc from data
        //....
    }, function(err){})
}


/*examples
.....To list objects under a model......
DS.list(<model_name>, {.....filter parameters.......}, function(data){}, function(e){});
///Sample Response
//{
    "is_first": true,
    "total_number": 10,
    "page_size": 50,
    "page_number": 1,
    "is_last": true,
    "data": [....]
}
-----------------------------
.....To get specific object.........
DS.get(<model_name>, "<object_id>", function(data){}, function(e){});
//Sample Response
//{.....}
-----------------------------
.....To get list of attributes in a model......
ds.attributes(<model_name>, function(data){}, function(e){})
//Sample Response of person model
//[
    {
        "required": false,
        "title": "Name",
        "placeholder": "Name",
        "type": "name",
        "name": "name"
    },
    {
        "required": true,
        "title": "Person Id",
        "placeholder": "Person Id",
        "type": "uid",
        "id": true,
        "name": "person_id"
    },
    ....
]
-----------------------------
.....To post/update/delete object.........
ds.post(<model_name>, {....}, function(data){}, function(e){});
ds.update(<model_name>,"<object_id>" ,{....}, function(data){}, function(e){});
ds.remove(<model_name>,"<object_id>", function(data){}, function(e){});
//Sample Respose
//{.....}
-----------------------------
.....To create login for 
DS.signup({...}, function(data){}, function(e){})
//Sample respose
//{.....}
//
*/

