import React, { useState, useEffect } from 'react';
import './basic_information.css';
import MaleGroup1 from '../../assets/img/Male/Group1.png';
import MaleGroup2 from '../../assets/img/Male/Group2.png';
import MaleGroup3 from '../../assets/img/Male/Group3.png';
import MaleGroup4 from '../../assets/img/Male/Group4.png';
import MaleGroup5 from '../../assets/img/Male/Group5.png';

import FemaleGroup1 from '../../assets/img/Female/Group1.png';
import FemaleGroup2 from '../../assets/img/Female/Group2.png';
import FemaleGroup3 from '../../assets/img/Female/Group3.png';
import FemaleGroup4 from '../../assets/img/Female/Group4.png';
import FemaleGroup5 from '../../assets/img/Female/Group5.png';

const BodyType = {
  Male: [{
    name: "Inverted triangle",
    image_url: MaleGroup1,
    description: "You have an athletic build, a well-developed chest and shoulders that are significantly broader than your waist and hips. Overall, the upper half of your body is wider than the lower half. Your shoulder, arm and chest muscles may be fairly bulky.",
    title: 'Inverted triangle',
  },
  {
    name: "Trapezoid",
    image_url: MaleGroup2,
    description: "You have broad shoulders and a well-developed chest. Your waist and hips are narrower but not disproportionately.",
    title: 'Trapezoid',
  },
  {
    name: "oval",
    image_url: MaleGroup3,
    description: "You're carrying a little extra weight on your body.  Your torso is wider than your shoulders and hips. You have a more rounded stomach, fuller face, and short neck. However, your legs are slimmer in comparison with your upper half.",
    title: 'oval',
  },
  {
    name: "Triangle",
    image_url: MaleGroup4,
    description: "You're larger around the waist and hips in relation to your chest. Your lower body is heavier than your upper body and you have narrow or sloped shoulders.",
    title: 'Triangle',
  },
  {
    name: "Rectangle",
    image_url: MaleGroup5,
    description: "Your shoulders are roughly the same width as your waist and hips. You have a slight or slender frame.",
    title: 'Rectangle',
  }
  ],
  Female: [{
    name: "Rectangle",
    image_url: FemaleGroup1,
    description: "The body looks very even with no obvious curves The chest, waist, and hip size are similar.",
    title: 'Rectangle',
  },
  {
    name: "Triangle",
    image_url: FemaleGroup2,
    description: "The shoulders are narrower than the hips. Hip, thigh, and buttock areas carry more weight than the rest of the body",
    title: 'Pear Shape',
  },
  {
    name: "Apple",
    image_url: FemaleGroup3,
    description: "The body tends to look round and have a wider waist due to fat build up around the abdomen It's more common in men.",
    title: 'Apple',
  },
  {
    name: "Hour Glass",
    image_url: FemaleGroup4,
    description: "Body fat is evenly distributed in the upper body and lower body The waist is small and well-defined",
    title: 'Hour Glass',
  },
  {
    name: "Inverted Triangle",
    image_url: FemaleGroup5,
    description: "The body has a larger bust or broad shoulders. The hip area is mrore narrow.",
    title: 'Inverted Triangle',
  }
  ]
}

function ChooseBodyType(props) {
  const [selectedBodyType, setSelectedBodyType] = useState(null);

  useEffect(() => {
    console.log('props=======>>>>>', props)
    if (props.userBodyType) {
      setSelectedBodyType(props.userBodyType)
    }
  }, []);

  const onSelectBodyType = (name, title) => {
    props.onSelectBody(false)
    props.onSelectType(name)
    props.onSelectTitle(title)
    setSelectedBodyType(name)
  }

  const renderBodyTypeCard = (item, index) => {
    return (
      <div className={`${selectedBodyType === item.name ? 'body_type_card_selected' : 'body_type_card'} text-center d-flex`} onClick={() => { onSelectBodyType(item.name, item.title) }}>
        <img alt="body_type" src={item.image_url} className="body_type_image" alt="" />
        <div className="body_type_details">
          <p className="body_type_text">{item.name}</p>
          <p className="body_type_description">{item.description}</p>
        </div>
      </div>
    )
  }

  return (
    <div>
      <p className="basic_information_title">Choose body type.</p>
      <div className="d-flex align-items-center flex-wrap mt-4 flex-column">
        {props.gender === 'Male' ?
          BodyType.Male.map((item, index) => {
            return (
              <div key={index}>
                {renderBodyTypeCard(item, index)}
              </div>
            )
          })
          :
          BodyType.Female.map((item, index) => {
            return (
              <div key={index}>
                {renderBodyTypeCard(item, index)}
              </div>
            )
          })
        }
      </div>
    </div>
  );
}
export default ChooseBodyType;