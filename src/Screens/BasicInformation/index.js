import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner';
import UploadImageBackground from '../../assets/img/upload_file_background.png';
import ChooseBodyType from './choose_body_type'
import DaveService, { makeid } from "../../daveservice";
import history from '../../history';
import Switch from 'react-input-switch';

let SIGNUP_KEY = "ZGFHZSAleHBvmtU5NgEyNzc0NyA1Ng__"
let ENTERPRISE_ID = "heidi_fashion_fit"
let HOST_URL = "https://dashboard.iamdave.ai"

let ds = new DaveService(HOST_URL, ENTERPRISE_ID, { "signup_key": SIGNUP_KEY });

let basic_information = JSON.parse(localStorage.getItem("basic_information"))
function BasicInformation(props) {

  var inToSM = (basic_information?.height / 2.54);
  var fh = (basic_information?.user_height_cm !== 'true' ? inToSM : basic_information?.height);


  const initialValues = {
    name: basic_information?.person_name ? basic_information?.person_name : '',
    email: basic_information?.email ? basic_information?.email : '',
    phoneNumber: basic_information?.mobile_number ? Number(basic_information?.mobile_number) : null,
    age: basic_information?.age ? Number(basic_information?.age) : null,
    height: basic_information?.height ? fh : null,
    weight: basic_information?.weight ? Number(basic_information?.weight) : null,
    sex: basic_information?.gender ? basic_information?.gender : '',
    bodyType: basic_information?.body_type ? basic_information?.body_type : '',
    uploadImageURL: basic_information?.profile_image ? basic_information?.profile_image : '',
    user_height_cm: basic_information?.user_height_cm ? basic_information?.user_height_cm : 'true',
  };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [uploadFileURL, setUploadFileURL] = useState('');
  const [selectedGender, setSelectedGender] = useState('');
  const [bodyTypeValidation, setBodyTypeValidation] = useState('');
  const [chooseBodyType, setChooseBodyType] = useState(false);
  const [userBodyType, setuserBodyType] = useState('');
  const [showSpinner, setShowSpinner] = useState(false);
  const [userDetails, setUserDetails] = useState();
  const [sizeControl, setSizeControl] = useState('true');
  

  var ann = false;
  if (props.location.state) {
    ann = props.location.state.anonymous || ann;
    console.log('props.location.state.anonymous==>>>>', props.location.state.anonymous, ann)
    //setShowSpinner(false)
  }
  const [anonymous,
    setAnonymous] = useState(ann);

  useEffect(() => {
    let basic_information = JSON.parse(localStorage.getItem("basic_information"))
    setuserBodyType(basic_information?.body_type ? basic_information?.body_type : '')
    setSelectedGender(basic_information?.gender ? basic_information?.gender : '')
  }, []);

  useEffect(() => {
    if (userDetails) {
      const user_gender = userDetails.gender.toLowerCase()
      ds.list('measurement_type', { '_sort_by': 'show_in_order', 'disable': 'false', 'measurement_for': user_gender }, function (data) {
        if (data.total_number !== 0) {
          localStorage.setItem("basic_information", JSON.stringify(userDetails));
          setShowSpinner(false)

          var priorityData = data.data
          // priorityData.sort(function (a, b) {
          //   return parseFloat(a.priority) - parseFloat(b.priority);
          // });
          localStorage.setItem("body_measurements", JSON.stringify(priorityData));
          const localStorageData = Object.entries(localStorage)

          if (localStorageData.length !== 0) {
            history.push({
              pathname: '/BodyMeasurements',
            })
            window.location.reload();
          }
        }
      }, function (err) {
        setShowSpinner(false)
      })
    }
  }, [userDetails]);

  const submitForm = () => {
    console.log(formValues);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setShowSpinner(true)
    setFormErrors(validate(formValues));
    setIsSubmitting(true);
    var error = validate(formValues)
    var sHeight = (formValues.height * 2.54)
    const sData = {
      "person_name": formValues.name,
      "mobile_number": !anonymous? formValues.phoneNumber: null,
      "email": anonymous? "dinesh+"+makeid(6)+"@iamdave.ai" : formValues.email,
      "gender": formValues.sex,
      "age": formValues.age,
      "weight": formValues.weight,
      "height": formValues.user_height_cm !== 'true' ? sHeight : formValues.height,
      "body_type": formValues.bodyType,
      "person_type": "volunteer",
      "collected_by": formValues.collected_by
    }
    console.log(":::submit form:::", sData)
    let userId = JSON.parse(localStorage.getItem("user_id"))
    if (
      !error.age &&
      !error.bodyType &&
      !error.email &&
      !error.height &&
      !error.name &&
      !error.phoneNumber &&
      !error.sex &&
      !error.uploadUserPhoto &&
      !error.weight
    ) {
      function _update(sData){
        ds.update("person", userId, { ...sData }, function (data) {
          localStorage.setItem("api_key", JSON.stringify(data.api_key));
          localStorage.setItem("user_id", JSON.stringify(data.user_id));
          data["user_height_cm"] = formValues.user_height_cm;
          setUserDetails(data)
        }, function (err) {
          console.log('getModel error=====>', err)
        })
      }
      if (userId) {
        if (uploadFileURL) {
          ds.upload_file(uploadFileURL, uploadFileURL.name, function (img_data) {
            if (img_data) {
              sData["profile_image"] = img_data.path;
              _update(sData);
            }
          }, function (err) {
            setShowSpinner(false)
            console.log('getModel error=====>', err)
          })
        } else {
          _update(sData);
        }
      } else {
        ds.signup(sData, function (data) {
          if (uploadFileURL) {
            ds.upload_file(uploadFileURL, uploadFileURL.name, function (img_data) {
              if (img_data) {
                ds.update("person", data.user_id, { 'profile_image': img_data.path, ...sData }, function (data) {
                  localStorage.setItem("api_key", JSON.stringify(data.api_key));
                  localStorage.setItem("user_id", JSON.stringify(data.user_id));
                  data["user_height_cm"] = sizeControl;
                  setUserDetails(data)
                }, function (err) {
                  console.log('getModel error=====>', err)
                })
              }
            }, function (err) {
              setShowSpinner(false)
              console.log('getModel error=====>', err)
            })
          } else {
            ds.update("person", data.user_id, { ...sData }, function (data) {
              localStorage.setItem("api_key", JSON.stringify(data.api_key));
              localStorage.setItem("user_id", JSON.stringify(data.user_id));
              data["profile_image"] = formValues.uploadImageURL;
              data["user_height_cm"] = formValues.user_height_cm;
              setUserDetails(data)
            }, function (err) {
              console.log('getModel error=====>', err)
            })
          }
        }, function (e) {
          console.log('signup error=====>', e)
          setShowSpinner(false)
        });
      }
    }
  }

  const validate = (values) => {
    let errors = {};
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;

    if(!anonymous){
      if (!values.email) {
        errors.email = "Please enter your email.";
      } else if (!regex.test(values.email)) {
        errors.email = "Invalid email format";
      } else {
        errors.email = "";
      }
    }else{
      errors.email = "";
    }

    if (!values.name) {
      errors.name = "Please enter your name.";
    } else if (values.name.length > 30) {
      errors.name = "Name must be 30 characters or less";
    } else if (values.name.length < 3) {
      errors.name = "Name must be 3 characters or more";
    } else {
      errors.name = "";
    }

    if(!anonymous){
      if (!values.phoneNumber) {
        errors.phoneNumber = "Please enter your phone number.";
      } else if (values.phoneNumber.length < 10) {
        errors.phoneNumber = "Please put 10 digit phone number";
      } else if (values.phoneNumber.length > 10) {
        errors.phoneNumber = "Phone number must be at least 10 digit";
      } else {
        errors.phoneNumber = "";
      }
    }else{
      errors.phoneNumber = "";
    }
    if (!values.age) {
      errors.age = "Please enter your age.";
    } else if (values.age > 150) {
      errors.age = "Age must be 150 or less";
    } else if (values.age < 5) {
      errors.age = "Age must be 5 or more";
    } else {
      errors.age = "";
    }

    if (!values.height) {
      errors.height = "Please enter your height.";
    } else {
      if (sizeControl === 'true') {
        if (values.height > 305) {
          errors.height = "Height must be 305 centimeter or less";
        } else if (values.height < 100) {
          errors.height = "Height must be 100 centimeter or more";
        } else {
          errors.height = "";
        }
      } else {
        if (values.height > 120) {
          errors.height = "Height must be 120 inch or less";
        } else if (values.height < 40) {
          errors.height = "Height must be 40 inch or more";
        } else {
          errors.height = "";
        }
      }
    }

    if (!values.weight) {
      errors.weight = "Please enter your weight.";
    } else if (values.weight > 200) {
      errors.weight = "Weight must be 200 or less";
    } else if (values.weight < 10) {
      errors.weight = "Weight must be 10 or more";
    } else {
      errors.weight = "";
    }

    if (!values.sex) {
      errors.sex = "Please select your sex";
    } else {
      errors.sex = "";
    }

    if (!values.bodyType) {
      errors.bodyType = "Please select your body type";
    } else {
      errors.bodyType = "";
    }

    errors.uploadUserPhoto = "";
    // if(!anonymous){
    //   if (!values.uploadImageURL) {
    //     errors.uploadUserPhoto = "Please upload your photo.";
    //   } else {
    //     errors.uploadUserPhoto = "";
    //   }
    // }else{
    //   errors.uploadUserPhoto = "";
    // }

    if (
      errors.age ||
      errors.bodyType ||
      errors.email ||
      errors.height ||
      errors.name ||
      errors.phoneNumber ||
      errors.sex ||
      errors.uploadUserPhoto ||
      errors.weight
    ) {
      setShowSpinner(false)
    }
    return errors;
  };

  useEffect(() => {
    if (Object.keys(formErrors).length === 0 && isSubmitting) {
      submitForm();
    }
  }, [formErrors]);


  const uploadUserProfile = (e) => {
    if (e.target.files.length !== 0) {
      setUploadFileURL(e.target.files[0])
      const data = {
        target: {
          name: 'uploadImageURL',
          value: URL.createObjectURL(e.target.files[0]),
        }
      }
      handleChange(data)
    }
  }
  const onSelectGender = (value) => {
    setSelectedGender(value)
    formValues.bodyType = ''
    if (selectedGender) {
      if (selectedGender !== value) {
        setuserBodyType('')
      }
    }
    if (bodyTypeValidation === 'Please select your sex.') {
      if (formValues.bodyType === '') {
        setBodyTypeValidation('Please select your body type.')
      } else {
        setBodyTypeValidation('')
      }
    } else {
      if (formValues.bodyType === '') {
        setBodyTypeValidation('Please select your body type.')
      } else {
        setBodyTypeValidation('')
      }
    }
  }

  const onClickBodyType = (value) => {
    if (formValues.sex === '') {
      setBodyTypeValidation('Please select your sex.')
    } else {
      setBodyTypeValidation('')
      if (value !== undefined && value !== '' && value !== null) {
        setChooseBodyType(value)
      } else {
        setChooseBodyType(true)

      }
    }
  }

  const onSelectedGender = (value) => {
    setSelectedGender(value)
  }

  const onSelectType = (value) => {
    setuserBodyType(value)
  }

  const onSelectTitle = (value) => {
    const data = {
      target: {
        name: 'bodyType',
        value: value,
      }
    }
    handleChange(data)
  }

  const addElements = ()=>{
    if(!anonymous){
      return (
        <>
          <div className="form-group">
              <div className="d-flex  align-items-center align-self-center">
                <label htmlFor="email" className="basic_information_input_title">Email</label>
                <p className="required_field_icon">*</p>
              </div>
              <input value={formValues.email} name="email" type="email" onChange={(e) => handleChange(e)}
                className={'form-control' + (formErrors.email ? ' is-invalid' : '')} />
              {formErrors.email && (
                <span className="input_error_message">{formErrors.email}</span>
              )}
            </div>

            <div className="form-group">
              <div className="d-flex  align-items-center align-self-center">
                <label htmlFor="phoneNumber" className="basic_information_input_title">Phone Number</label>
                <p className="required_field_icon">*</p>
              </div>
              <input value={formValues.phoneNumber} name="phoneNumber" type="number"
                onChange={(e) => handleChange(e)}
                className={'form-control' + (formErrors.phoneNumber ? ' is-invalid' : '')} />
              {formErrors.phoneNumber && (
                <span className="input_error_message">{formErrors.phoneNumber}</span>
              )}
            </div>
          </>
      )
    }
    return (<></>);
  }


  const addUploadImage = ()=>{
    if(!anonymous){
      return (
        <>
          
          <div>
              <div className="d-flex align-items-center align-self-center">
                <label htmlFor="uploadUserPhoto" className="basic_information_input_title">Upload full body image facing the camera</label>
                <p className="required_field_icon">*</p>
              </div>
              <label
                htmlFor="formBasicUploadYourPhoto"
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  position: 'relative',
                  height: formValues.uploadImageURL !== '' ? 250 : 168,
                  width: 'auto',
                  borderRadius: 10,
                  marginBottom: 0
                }}>
                <input
                  name="uploadUserPhoto"
                  type="file"
                  accept="image/*"
                  id="formBasicUploadYourPhoto"
                  className="d-none"
                  onChange={(e) => {
                    uploadUserProfile(e)
                  }} />
                <img
                  style={{
                    width: formValues.uploadImageURL !== '' ? null : '100%'
                  }}
                  className={`${formErrors.uploadUserPhoto ? 'upload_image_error' : 'upload_image'}`}
                  src={formValues.uploadImageURL !== ''
                    ? formValues.uploadImageURL
                    : UploadImageBackground}
                  alt="UploadImageBackground" />
              </label>
              {
                formErrors.uploadUserPhoto && (
                  <span className="input_error_message">{formErrors.uploadUserPhoto}</span>
                )
              }
            </div>
        </>
      )
    }
    return (<></>);
  }

  const handleChangeSize = (checked) => {
    setSizeControl(checked)
    const data = {
      target: {
        name: 'user_height_cm',
        value: checked,
      }
    }
    handleChange(data)

  }

  const handleChangeGender = (value) => {
    onSelectGender(value)
    const data = {
      target: {
        name: 'sex',
        value: value,
      }
    }
    handleChange(data)
  }

  console.log('selectedGender=======>>>>', selectedGender)

  return (
    <div className="basic_information">
      <div className="mt-4 basic_information_main">
        <div className={`${chooseBodyType === true ? 'index_show' : 'index_hide'}`}>
          <ChooseBodyType
            onSelectBody={(e) => onClickBodyType(e)}
            onSelectedGender={(e) => onSelectedGender(e)}
            onSelectType={(e) => onSelectType(e)}
            onSelectTitle={(e) => onSelectTitle(e)}
            gender={selectedGender}
            userBodyType={userBodyType}
          />
        </div>
        <div className={`${chooseBodyType === false ? 'index_show' : 'index_hide'}`}>
          <p className="basic_information_title">Basic Information</p>
          <form method="post" onSubmit={handleSubmit}>
            <div className="form-group">
              <div className="d-flex  align-items-center align-self-center">
                <label htmlFor="collected_by" className="basic_information_input_title">Measurements Collected by</label>
              </div>
              <input name="collected_by" type="text"
                value={formValues.collected_by}
                onChange={(e) => handleChange(e)}
                className={'form-control'} />
              
            </div>
            <div className="form-group">
              <div className="d-flex  align-items-center align-self-center">
                <label htmlFor="name" className="basic_information_input_title">Name</label>
                <p className="required_field_icon">*</p>
              </div>
              <input name="name" type="text"
                value={formValues.name}
                onChange={(e) => handleChange(e)}
                className={'form-control' + (formErrors.name ? ' is-invalid' : '')} />
              {formErrors.name && (
                <span className="input_error_message">{formErrors.name}</span>
              )}
            </div>

            {addElements()}
            <div className="form-group">
              <div className="d-flex  align-items-center align-self-center">
                <label htmlFor="age" className="basic_information_input_title">Age (Year)</label>
                <p className="required_field_icon">*</p>
              </div>
              <input value={formValues.age} name="age" type="number"
                onChange={(e) => handleChange(e)}
                className={'form-control' + (formErrors.age ? ' is-invalid' : '')} />
              {formErrors.age && (
                <span className="input_error_message">{formErrors.age}</span>
              )}
            </div>
            <div className="d-flex">
              <div className="form-group col-md-6 pl-0 pr-1">
                <div className="d-flex  align-items-center align-self-center">
                  <label htmlFor="height" className="basic_information_input_title">Height ({sizeControl === 'true' ? 'cm' : 'in'})</label>
                  <p className="required_field_icon">*</p>
                </div>
                <div className={formErrors.height ? 'error_input_form_control' : 'input_form_control'}>
                  <input value={formValues.height} name="height" type="number"
                    onChange={(e) => handleChange(e)}
                    className={'form_control' + (formErrors.height ? ' is-invalid' : '')} />
                  <div className="border_left_class" />
                  <div className="height_size_type_control">
                    <p>in</p>
                    <Switch style={{ marginTop: 5 }} on="true" off="false" onChange={(e) => { handleChangeSize(e) }} value={formValues.user_height_cm} />
                    <p>cm</p>
                  </div>
                </div>
                <div>
                </div>
                {formErrors.height && (
                  <span className="input_error_message">{formErrors.height}</span>
                )}
              </div>
              <div className="form-group col-md-6 pr-0 pl-1">
                <div className="d-flex  align-items-center align-self-center">
                  <label htmlFor="weight" className="basic_information_input_title">Weight (Kg)</label>
                  <p className="required_field_icon">*</p>
                </div>
                <input value={formValues.weight} name="weight" type="number"
                  onChange={(e) => handleChange(e)}
                  className={'form-control' + (formErrors.weight ? ' is-invalid' : '')} />
                {formErrors.weight && (
                  <span className="input_error_message">{formErrors.weight}</span>
                )}
              </div>
            </div>

            <div className="d-flex">
              <div className="form-group col-md-6 pl-0 pr-1">
                <div className="d-flex  align-items-center align-self-center">
                  <label htmlFor="sex" className="basic_information_input_title">Sex</label>
                  <p className="required_field_icon">*</p>
                </div>
                <select
                  onChange={(e) => { handleChangeGender(e.target.value) }}
                  as="select"
                  name="sex"
                  value={formValues.sex}
                  className={'form-control' + (formErrors.sex && selectedGender === '' ? ' is-invalid' : '')}>
                  <option value="">Choose...</option>
                  <option value="Male">Male</option>
                  <option value="Female">Female</option>
                </select>
                {selectedGender === '' &&
                  formErrors.sex && (
                    <span className="input_error_message">{formErrors.sex}</span>
                  )}
              </div>

              <div className="form-group col-md-6 pr-0 pl-1">
                <div className="d-flex  align-items-center align-self-center">
                  <label htmlFor="bodyType" className="basic_information_input_title">Body Type</label>
                  <p className="required_field_icon">*</p>
                </div>
                <div onClick={() => { onClickBodyType() }}>
                  <input name="bodyType" type="text"
                    style={{ backgroundColor: '#FFFFFF' }}
                    className={'form-control' + (formErrors.bodyType ? ' is-invalid' : '')}
                    value={formValues.bodyType}
                  />
                  {formValues.bodyType === '' ?
                    <div className="input_error_message " style={{ color: 'red' }}>
                      {bodyTypeValidation}
                    </div>
                    :
                    formErrors.bodyType && (
                      <span className="input_error_message">{formErrors.bodyType}</span>
                    )
                  }
                </div>
              </div>
            </div>
            {addUploadImage()}

            <div className="next_btn justify-content-center d-flex">
              <Button variant="primary" type="submit" disabled={showSpinner}>
                {showSpinner ?
                  <Spinner animation="border" role="status">
                    <span className="sr-only">Loading...</span>
                  </Spinner>
                  :
                  'Next'}
              </Button>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default BasicInformation;
