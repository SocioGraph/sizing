import React from 'react';
import './welcome.css';
import history from '../../history'
import Initialpageimage from "../../assets/img/Initialpageimage.png";

class Welcome extends React.Component{
  state = {
    checked: true
  };

  onStart = () => {
    history.push({pathname: '/BasicInformation', state: {anonymous: !this.state.checked}});
  }

  constructor(props){
    super(props);
  }
  
  onChange_(v){
    this.setState({
      checked: !this.state.checked
    });
    
    console.log(this.state.checked);
    return true;
  }
  render(){
    return (
      <div className="welcome_screen">
        <div className="mt-3 welcome_screen_main">
          <div className="d-flex justify-content-center flex-column align-items-center">
          <img alt="Initialpageimage" src={Initialpageimage} className="welcome_background" />
          <div className="welcome_title_main">
            <div className="welcome_title">
              <p className="welcome_title_1">Welcome to "HEIDI - Highly Ergonomic Indian Dimensions' Initiative".</p>
              <p className="welcome_title_2">We help you easily find the right fit based on your body measurements. - Bcz Style has no Size!</p>
            </div>
          </div>
          <div className="welcome_description">
            <p className="welcome_description_1">
              We are collecting sizing information about Indian body types and measurements so that we can create an Indian standard sizing chart and create a bunch of Artificial Intelligence based applications to find a perfect fit for you.
          </p>
            <p className="welcome_description_2">
              Please spend 10 mins of your time to provide your measurements & help us with this initiative. If possible, please have a friend or family member along with you to take the measurements and picture.
          </p>
            </div>
            <div>
              <br/>
              <br/>
              <p>
              <input type="checkbox" checked={this.state.checked} onChange={()=>this.onChange_()}  style={{ 'height' : '15px', 'width' : '15px', 'backgroundColor' : '#eee'}}></input> <span>By clicking the above button you are accepting the below Privacy Policy.</span>
              </p>
              <p>"The personal data you share with us will be used by DaveAI for R&D purposes only. Anonymized data (i.e. data excluding, images, names and contact details) may be shared with other R&D institutes."</p>
            </div>
            <div className="justify-content-center d-flex">
              <div className="start_btn" onClick={this.onStart}>
                <p className="start_btn_title">START</p>
              </div>
              </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Welcome;
