import React, { useState, useEffect } from 'react';
import './body_measurements.css';
import Spinner from 'react-bootstrap/Spinner';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Toast from 'react-bootstrap/Toast';
import history from '../../history';
import DaveService from "../../daveservice";

let SIGNUP_KEY = "ZGFHZSAleHBvmtU5NgEyNzc0NyA1Ng__"
let ENTERPRISE_ID = "heidi_fashion_fit"
let HOST_URL = "https://dashboard.iamdave.ai"

let ds = new DaveService(HOST_URL, ENTERPRISE_ID, { "signup_key": SIGNUP_KEY });
let body_measurements = JSON.parse(localStorage.getItem("body_measurements"))

function BodyMeasurements(props) {
  const [showError, setShowError] = useState(false);
  const [sizeValue, setSetSizeValue] = useState(null);
  const [showSpinner, setShowSpinner] = useState(true);
  const [bodyMeasurements, setBodyMeasurements] = useState([]);
  const [currentScreen, setCurrentScreen] = useState(1);
  const [sizeControl, setSizeControl] = useState(null);
  const [isLastIndex, setIsLastIndex] = useState();
  const [measurementId, setMeasurementId] = useState('');
  const [showSubmitSpinner, setShowSubmitSpinner] = useState(false);
  const [showToast, setShowToast] = useState(false);
  const [showToastType, setToastType] = useState('');
  const [showToastMessage, setToastMessage] = useState('');
  const [showModal, setShowModal] = useState(false);
  const [sizeErrorMessage, setSizeErrorMessage] = useState('');

  useEffect(() => {
    setBodyMeasurements([])
    let myGreeting = setTimeout(() => {
      if (body_measurements) {
        setBodyMeasurements(body_measurements)
        setShowSpinner(false)
      } else {
        setShowSpinner(false)
        history.push('/BasicInformation')
      }
    }, 2000);
    return myGreeting
  }, []);

  useEffect(() => {
    if (isLastIndex !== undefined && isLastIndex !== null && isLastIndex !== '' && measurementId !== '') {
      const sizeData = sizeValue !== null ? sizeValue : bodyMeasurements[isLastIndex].noted_value
      bodyMeasurements[isLastIndex] = {
        ...bodyMeasurements[isLastIndex],
        noted_value: Number(sizeData),
        sizeControl: sizeControl,
        measurement_id: measurementId,
        value: {
          in: sizeControl ? Number((sizeData / 2.54).toFixed(2)) : Number(sizeData),
          cm: sizeControl ? Number(sizeData) : Number((sizeData * 2.54).toFixed(2)),
        }
      }
      setShowSubmitSpinner(false)
      setShowError(false)
      setSetSizeValue(null)
      setCurrentScreen(currentScreen + 1)
      setIsLastIndex()
      setSizeControl(null)
      setMeasurementId('')
      localStorage.setItem("body_measurements", JSON.stringify(body_measurements));
      setBodyMeasurements(body_measurements)
      if (bodyMeasurements.length === currentScreen) {
        props.history.push({
          pathname: '/ViewSummary',
          state: { lasiSizeControl: sizeControl }
        })
      }
    }
  }, [measurementId, isLastIndex]);

  const autoCloseToast = () => {
    setTimeout(function () {
      setShowToast(false);
      setToastType('');
      setToastMessage('');
    }, 3000)
  }

  const handleSubmit = (item) => {
    setShowSubmitSpinner(true)
    var isSize = sizeControl !== null ? sizeControl : item.sizeControl !== undefined ? item.sizeControl : true
    setSizeControl(isSize)
    let user_id = JSON.parse(localStorage.getItem("user_id"))
    const sizeData = sizeValue !== null ? sizeValue : item.noted_value
    if (!sizeData) {
      setShowSubmitSpinner(false)
      setShowError(true)
      setSizeErrorMessage('');
    } else {
      if (sizeData <= 0) {
        setShowError(true)
        setShowSubmitSpinner(false)
        setSizeErrorMessage('Please enter valid size');
      } else {
        setSizeErrorMessage('');
        var Size = isSize === true ? sizeData : isSize === false ? (sizeData * 2.54) : sizeData
        // const num = Number(Size)
        const finalSize = Size;
        if (bodyMeasurements) {
          if (bodyMeasurements.length !== 0) {
            for (var i = 0; i < bodyMeasurements.length; i++) {
              if (item.measurement_name === bodyMeasurements[i].measurement_name) {
                setIsLastIndex(i)
                if (bodyMeasurements[i].measurement_id) {
                  bodyMeasurements[i] = {
                    ...bodyMeasurements[i],
                    noted_value: sizeData,
                    sizeControl: isSize,
                  }
                  ds.update("measurement", bodyMeasurements[i].measurement_id, { 'noted_value': finalSize }, function (data) {
                    setMeasurementId(data.measurement_id)
                  }, function (err) {
                    setShowSubmitSpinner(false)
                    setToastType('Error')
                    setShowToast(true)
                    autoCloseToast()
                    setToastMessage(err.statusText)
                    console.log('getModel error=====>', err)
                  })
                } else {
                  ds.post("measurement", { 'person_id': user_id, 'noted_value': finalSize, 'measurement_type_id': item.measurement_type_id }, function (data) {
                    setMeasurementId(data.measurement_id)
                  }, function (err) {
                    setShowSubmitSpinner(false)
                    setToastType('Error')
                    setShowToast(true)
                    autoCloseToast()
                    setToastMessage(err.statusText)
                    console.log('getModel error=====>', err)
                  })
                }
              }
              // if (body_measurements.length - 1 === i) {
              // body_measurements[i] = {
              //   ...body_measurements[i],
              //   noted_value: Size,
              //   sizeControl: sizeControl,
              //   measurement_id: measurementId
              // }
              // setShowError(false)
              // setSetSizeValue('')
              // setCurrentScreen(currentScreen + 1)
              // localStorage.setItem("body_measurements", JSON.stringify(body_measurements));
              // }
            }
          }
        }
      }
    }
  };

  const onBackPress = () => {
    if (currentScreen === 1) {
      history.goBack()
      setShowSubmitSpinner(false)
    } else {
      setShowSubmitSpinner(false)
      setCurrentScreen(currentScreen - 1)
      setSetSizeValue(null)
      setSizeControl(null)
      setShowError(false)
    }
  }

  const onSubmiteText = (value) => {
    const data = Number(value)
    if (data) {
      setShowError(false)
      setSetSizeValue(data)
    } else {
      setSetSizeValue('')
      setShowError(true)
    }
  }

  const changeSizeControl = (value) => {
    if (sizeControl !== null) {
      setSizeControl(!sizeControl)
    } else if (value !== undefined && value !== null && value !== '') {
      setSizeControl(!value)
    } else {
      setSizeControl(false)
    }
  }

  const checkedValue = (value) => {
    // sizeControl !== null ? sizeControl : item.sizeControl !== '' || item.sizeControl !== undefined || item.sizeControl !== null ? item.sizeControl : true
    if (sizeControl !== null) {
      return sizeControl
    } else if (value !== '' && value !== undefined && value !== null) {
      return value
    } else {
      return true
    }
  }

  const handleClose = () => setShowModal(false);

  const handleNoPress = () => {
    setShowModal(false)
  };
  const handleYesPress = () => {
    setShowModal(false)
    setCurrentScreen(currentScreen + 1)
    if (bodyMeasurements.length === currentScreen) {
      props.history.push({
        pathname: '/ViewSummary',
        state: { lasiSizeControl: sizeControl }
      })
    }
  };

  const onSkipmeasurement = (item, index) => {
    if (item.priority === 2) {
      setShowModal(true)
    } else if (body_measurements.length === index + 1) {
      props.history.push({
        pathname: '/ViewSummary',
        state: { lastSizeControl: item.sizeControl }
      })
    } else {
      setCurrentScreen(currentScreen + 1)
    }
  }

  return (
    <div className="basic_information">
      <div className="mt-4 basic_information_main">
        {showToast &&
          <div className="position-fixed w-100 d-flex justify-content-end mr-5">
            <div className={`d-flex justify-content-end mr-4 ${showToastType === 'Error' ? 'error_tost' : 'success_tost'}`}>
              <Toast>
                <Toast.Header>
                  <img alt="holder" src="holder.js/20x20?text=%20" className="rounded mr-0" alt="" onClick={() => { setShowToast(false) }} />
                  <strong className="mr-auto">{showToastType === 'Error' ? 'Error' : 'Success'}</strong>
                </Toast.Header>
                <Toast.Body>{showToastMessage}</Toast.Body>
              </Toast>
            </div>
          </div>
        }
        {showSpinner ?
          (
            <div className="d-flex justify-content-center align-items-center" style={{ height: '100vh', paddingTop: 20 }}>
              <Spinner animation="border" role="status">
                <span className="sr-only">Loading...</span>
              </Spinner>
            </div>
          ) : (
            <div className="input">
              {bodyMeasurements?.map((item, index) => {
                if (currentScreen === index + 1) {
                  return (
                    <div key={index}>
                      <div className="basic_information_title_div">
                        <div className="d-flex justify-content-center align-items-center align-self-center">
                          <p className="basic_information_title">{item.measurement_name}</p>
                          <p className="required_field_icon">*</p>
                        </div>
                        <p className="basic_information_input_title">{currentScreen} / {bodyMeasurements.length}</p>
                      </div>
                      <p className="basic_information_input_description">{item.measurement_desc}</p>
                      <img alt="measurement_ref" src={item.measurement_ref} className="body_image" />
                      <div className={showError ? 'error_input_form_control' : 'input_form_control'}>
                        <input
                          className="form_control"
                          type="number"
                          value={sizeValue !== null ? sizeValue : item.noted_value}
                          placeholder="Enter your size centimeters"
                          onChange={(e) => { onSubmiteText(e.target.value.trim()) }}
                        />
                        <div className="border_left_class" />
                        <div className="size_type_control">
                          {/* <p>in</p>
                          <Form.Check
                            checked={checkedValue(item.sizeControl)}
                            onChange={(e) => { changeSizeControl(item.sizeControl) }}
                            type="switch"
                            id="size-type-control"
                          /> */}
                          <p>cm</p>
                        </div>
                      </div>
                      {sizeErrorMessage && (
                        <span className="input_error_message">{sizeErrorMessage}</span>
                      )}
                      <div className='back_submit_btn d-flex justify-content-between'>
                        <Button variant="primary" type="button" onClick={() => { onBackPress() }}>
                          Back
                        </Button>
                        <Button variant="primary" type="submit" disabled={showSubmitSpinner} onClick={() => { handleSubmit(item) }}>
                          {showSubmitSpinner ?
                            <Spinner style={{ height: 25, width: 25 }} animation="border" role="status">
                              <span className="sr-only">Loading...</span>
                            </Spinner>
                            :
                            body_measurements.length !== index + 1 ? 'Submit' : 'View Summary'
                          }
                        </Button>
                      </div>
                      {item.priority !== 1 &&
                        <div className="d-flex justify-content-end">
                          <p className="mt-2 skip_measurement" onClick={() => { onSkipmeasurement(item, index) }} >Skip</p>
                        </div>
                      }
                    </div>
                  )
                }
              })}
            </div>
          )}
      </div>
      <Modal
        show={showModal}
        onHide={handleClose}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Body className="justify-content-center d-flex">Are you sure you want to skip this measurement? This measurement is important!!</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleNoPress}>
            No
          </Button>
          <Button variant="primary" onClick={handleYesPress}>
            Yes
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}
export default BodyMeasurements;
