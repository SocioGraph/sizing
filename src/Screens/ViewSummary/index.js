import React, { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner';
import Switch from 'react-input-switch';
import Modal from 'react-bootstrap/Modal';
import './view_summary.css';
import history from '../../history'
import userImage from "../../assets/img/userImage.png";
import DaveService from "../../daveservice";
let SIGNUP_KEY = "ZGFHZSAleHBvmtU5NgEyNzc0NyA1Ng__"
let ENTERPRISE_ID = "heidi_fashion_fit"
let HOST_URL = "https://dashboard.iamdave.ai"

let ds = new DaveService(HOST_URL, ENTERPRISE_ID, { "signup_key": SIGNUP_KEY });

function ViewSummary(props) {
  const [viewSummaryData, setViewSummaryData] = useState([]);
  const [basicInformation, setBasicInformation] = useState({});
  const [showSpinner, setShowSpinner] = useState(true);
  const [showDataSpinner, setShowDataSpinner] = useState(false);
  const [sizeControl, setSizeControl] = useState(true);
  const [isUpdateSizeControl, setIsUpdateSizeControl] = useState(null);
  const [sendDataIndex, setSendDataIndex] = useState(0);
  const [isError, setIsError] = useState(false);
  const [showModal, setShowModal] = useState(false);


  useEffect(() => {
    let user_id = JSON.parse(localStorage.getItem("user_id"))
    if (user_id) {
      setShowSpinner(false)
    } else {
      setShowSpinner(false)
      history.push('/BasicInformation')
    }
  }, []);

  useEffect(() => {
    console.log('props====>>>', props)
    let body_measurements = JSON.parse(localStorage.getItem("body_measurements"))
    let basic_information = JSON.parse(localStorage.getItem("basic_information"))
    let size_control = JSON.parse(localStorage.getItem("size_control"))
    setViewSummaryData(body_measurements);
    if (props?.location?.state !== null && props?.location?.state !== undefined && props?.location?.state !== '') {
      setSizeControl(props.location.state.lasiSizeControl)
      if (props.location.state.lasiSizeControl === false) {
        for (var i = 0; i < body_measurements.length; i++) {
          body_measurements[i].sizeControl = false
          // if (body_measurements[i].noted_value) {
          //   body_measurements[i].noted_value = (body_measurements[i].noted_value * 2.54).toFixed(2)
          // }
        }
      } else {
        for (var i = 0; i < body_measurements.length; i++) {
          body_measurements[i].sizeControl = props.location.state.lasiSizeControl
        }
      }
    } else {
      for (var i = 0; i < body_measurements.length; i++) {
        body_measurements[i].sizeControl = true
      }
      setSizeControl(true)
    }
    setBasicInformation(basic_information)
    if (size_control) {
      setSizeControl(size_control)
    }
  }, []);

  useEffect(() => {
    if (sendDataIndex) {
      if (!isError) {
        if (sendDataIndex === viewSummaryData.length) {
          setShowDataSpinner(false)
          localStorage.setItem("size_control", JSON.stringify(sizeControl));
          localStorage.setItem("body_measurements", JSON.stringify(viewSummaryData));
          history.push('/Participating')
        }
      }
    }
  }, [sendDataIndex, isError]);

  useEffect(() => {
    if (showModal === true) {
      let myGreeting = setTimeout(() => {
        setShowModal(false)
      }, 2500);
      return myGreeting
    }
  }, [showModal]);

  const onEditPress = () => {
    let basic_information = JSON.parse(localStorage.getItem("basic_information"))
    const user_gender = basic_information.gender
    ds.list('measurement_type', { '_sort_by': 'show_in_order', 'disable': 'false', 'measurement_for': user_gender }, function (data) {
      if (data.total_number !== 0) {
        history.push({
          pathname: '/BodyMeasurements',
        })
      }
    })
  }
  const handleSubmit = () => {
    history.push('/Participating')
  }

  const handleChangeSize = (checked) => {
    setIsUpdateSizeControl(true)
    setSizeControl(checked)
    for (var i = 0; i < viewSummaryData.length; i++) {
      viewSummaryData[i].sizeControl = checked === true ? true : false
      //  if (checked === true && viewSummaryData[i].noted_value) {
      //    viewSummaryData[i].noted_value = (viewSummaryData[i].noted_value / 2.54).toFixed(2)
      //  } else {
      //    if (viewSummaryData[i].noted_value) {
      //      viewSummaryData[i].noted_value = (viewSummaryData[i].noted_value * 2.54).toFixed(2)
      //    }
      //  }
    }
  }

  const toFeet = (n) => {
    var inToSM = (basicInformation.height / 2.54);
    var fh = (basicInformation?.user_height_cm !== true ? inToSM : basicInformation?.height);
    var realFeet = ((fh * 0.393700) / 12);
    var feet = Math.floor(realFeet);
    var inches = Math.round((realFeet - feet) * 12);
    return feet + "." + inches + '"';
  }

  const handleClose = () => setShowModal(false);

  return (
    <div className="basic_information">
      <div className="mt-3 basic_information_main">
        {showSpinner ?
          (
            <div className="d-flex justify-content-center align-items-center" style={{ height: '100vh', paddingTop: 20 }}>
              <Spinner animation="border" role="status">
                <span className="sr-only">Loading...</span>
              </Spinner>
            </div>
          ) : (
            <>
              <Modal
                show={showModal}
                onHide={handleClose}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
              >
                <Modal.Body className="justify-content-center d-flex">Received an error while connecting to server. Please try again.</Modal.Body>
              </Modal>
              {showDataSpinner && (
                <div className="d-flex justify-content-center align-items-center" style={{ position: 'Fixed', justifyContent: 'center', left: 0, top: 0, right: 0, bottom: 0 }}>
                  <Spinner animation="border" role="status">
                    <span className="sr-only">Loading...</span>
                  </Spinner>
                </div>
              )}
              {basicInformation &&
                <div className="user_details_view">
                  <div className="col-md-5 user_details p-0">
                    <div className="summary_border_bottom">
                      <p className="user_details_title">Name</p>
                      <p className="user_details_value">{basicInformation.person_name}</p>
                    </div>
                    <div className="summary_border_bottom">
                      <p className="user_details_title">Body type</p>
                      <p className="user_details_value">{basicInformation.body_type}</p>
                    </div>
                    <div className="summary_border_bottom d-flex justify-content-between p-0">
                      <div className="summary_border_right col-md-6 p-0 ">
                        <p className="user_details_title">Sex</p>
                        <p className="user_details_value">{basicInformation.gender}</p>
                      </div>
                      <div className="col-md-6 pr-0">
                        <p className="user_details_title">Height</p>
                        <p className="user_details_value">{basicInformation?.height} cm</p>
                      </div>
                    </div>
                    <div className="d-flex justify-content-between">
                      <div className="summary_border_right col-md-6 p-0 ">
                        <p className="user_details_title">Age</p>
                        <p className="user_details_value">{basicInformation.age} Year</p>
                      </div>
                      <div className="col-md-6 pr-0">
                        <p className="user_details_title">Weight</p>
                        <p className="user_details_value">{basicInformation.weight} Kg</p>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-7 pr-0">
                    <img alt="user_image" src={basicInformation.profile_image ? basicInformation.profile_image : userImage} className="user_image" />
                  </div>
                </div>
              }
              <div className="summary_card">
                <p className="summary_card_value">Measurement Name</p>
                <div className="height_type_control" style={{ width: 80 }}>
                  <p className="summary_card_value">in</p>
                  <Switch disabled={showDataSpinner} style={{ marginTop: 10 }} on={true} off={false} onChange={(e) => { handleChangeSize(e) }} value={sizeControl} />
                  <p className="summary_card_value">cm</p>
                </div>
              </div>
              {viewSummaryData?.map((item, index) => {
                return (
                  <div key={index} className="summary_card">
                    <p className="summary_card_title">{item.measurement_name}</p>
                    {
                      item.noted_value && (
                        <p className="summary_card_value">{item.sizeControl ? item.value.cm : item.value.in} {item.sizeControl ? 'cm' : 'in'}</p>
                      )}
                  </div>
                )
              })}
              <div className="edit_submit_btn d-flex justify-content-between">
                <Button disabled={showDataSpinner} variant="primary" type="button" onClick={() => { onEditPress() }}>
                  Edit
                </Button>
                <Button disabled={showDataSpinner} variant="primary" type="submit" onClick={() => { handleSubmit() }}>
                  Submit
                </Button>
              </div>
            </>
          )
        }
      </div>
    </div>
  );
}
export default ViewSummary;
