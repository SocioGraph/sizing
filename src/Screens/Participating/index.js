import React, { useState, useEffect } from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import './participating.css';
import EndPageBG from "../../assets/img/EndPageBG.png";
import history from "../../history";


function Participating() {
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    let myGreeting = setTimeout(() => {
      setShowModal(true)
    }, 1000);
    return myGreeting
  }, []);

  const handleClose = () => setShowModal(false);
  const handleNoPress = () => {
    history.push('./BodyMeasurements')
    setShowModal(false)
    window.location.reload();
  };
  const handleYesPress = () => {
    localStorage.clear()
    setShowModal(false)
    history.push('./BasicInformation')
    window.location.reload();
  };

  return (
    <div className="participating">
      <div className="participating_main">
        <img alt="EndPageBG" src={EndPageBG} className="participating_background" />
        <div className="participating_title">
          <p>Thank you for participating</p>
        </div>
      </div>
      <Modal
        show={showModal}
        onHide={handleClose}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Body className="justify-content-center d-flex">Do you want to get measurements for another person?</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleNoPress}>
            No
          </Button>
          <Button variant="primary" onClick={handleYesPress}>
            Yes
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}
export default Participating;
