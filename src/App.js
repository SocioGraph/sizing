import React from 'react';
import './App.css';
import Routes from './Routes';
class App extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Routes />
      </React.Fragment>
    );
  }
}

export default App;
