import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import history from './history';
import Welcome from './Screens/Welcome';
import BasicInformation from './Screens/BasicInformation';
import BodyMeasurements from './Screens/BodyMeasurements';
import ViewSummary from './Screens/ViewSummary';
import Participating from './Screens/Participating';
import { AuthContext } from './context/auth';

function Routes() {
    return (

        <AuthContext.Provider>
            <Router history={history}>
                <Switch>
                    <Route path='/' exact component={Welcome} />
                    <Route path='/BasicInformation' exact component={BasicInformation} />
                    <Route path='/BodyMeasurements' exact component={BodyMeasurements} />
                    <Route path='/ViewSummary' exact component={ViewSummary} />
                    <Route path='/Participating' exact component={Participating} />
                </Switch>
            </Router>
        </AuthContext.Provider>
    )
}

export default Routes;
